import * as Phaser  from "phaser";
import "./fonts/font.css";
import Resize from "./resize";
import firstScene from "./scenes/first-scene";
import gameOver from './scenes/game-over-scene';
import testScene from './scenes/test-scene/test-scene';

window.onload = () => {
  let config:Phaser.Types.Core.GameConfig = {
    type: Phaser.WEBGL,
    parent: "phaser-example",
    width: 1280,
    height: 720,
    pixelArt: true,
    input: {
      gamepad: true
    },
    scene: [firstScene, testScene, gameOver],
    physics: {
      default: 'arcade',
      arcade: {
        gravity: { y: 0 }
      }
    }
  };
  
  const game = new Phaser.Game(config);
  const resize = new Resize(game);
  resize.resize();
  window.addEventListener('resize', resize.resize, false);
}
