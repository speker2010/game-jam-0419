import InputPlugin = Phaser.Input.InputPlugin;

export default class Controls {
    input: InputPlugin;
    cursorKeys: CursorKeys;
    keys: Keys;
    gamepad: Phaser.Input.Gamepad.Gamepad;

    constructor(input: InputPlugin) {
        this.input = input;
        this.cursorKeys = input.keyboard.createCursorKeys() as CursorKeys;
        this.keys = input.keyboard.addKeys('W,A,S,D') as Keys;
        this.input.gamepad.once('down', (pad, button, index) => {
            this.gamepad = pad;
        });
        this.input.gamepad.on('down', (pad, button, index) => {
            console.log(pad, button, index);
        });
    }

    isStart() : boolean {
        let isGamepadStart : boolean = !!(this.gamepad && this.gamepad.A);
        return this.input.activePointer.isDown || isGamepadStart;
    }

    isFire() : boolean {
        let isGamepadFire : boolean = !!(this.gamepad && this.gamepad.R2);
        return this.cursorKeys.space.isDown || isGamepadFire;
    }

    isRotateGunsLeft() : boolean {
        let isGamepadLeft = this.gamepad && this.roundStickValue(this.gamepad.leftStick.x) < 0;
        return this.keys.A.isDown || isGamepadLeft;
    }

    isRotateGunsRight() : boolean {
        let isGamepadRight = this.gamepad && this.roundStickValue(this.gamepad.leftStick.x) > 0;
        return this.keys.D.isDown || isGamepadRight;
    }

    isRotateBodyLeft() : boolean {
        let isGamepadLeft = this.gamepad && this.roundStickValue(this.gamepad.rightStick.x) < 0;
        return this.cursorKeys.left.isDown || isGamepadLeft;
    }

    isRotateBodyRight() : boolean {
        let isGamepadRight = this.gamepad && this.roundStickValue(this.gamepad.rightStick.x) > 0;
        return this.cursorKeys.right.isDown || isGamepadRight;
    }

    isMove() : boolean {
        let isGamepadMove : boolean = !!(this.gamepad && this.gamepad.L2);
        return this.cursorKeys.up.isDown || isGamepadMove;
    }

    roundStickValue(value: number) : number {
        return Math.round(value);
    }
}
