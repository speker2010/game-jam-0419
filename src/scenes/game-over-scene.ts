import * as Phaser  from 'phaser';
import Controls from "../components/Controls";

export default class FirstScene extends Phaser.Scene {
    public message;
    controls: Controls;

    constructor() {
        super('GameOver');
    }

    init(data) {
        this.message = data.message;
    }

    preload() {
        
    }

    create() {    
        const text = this.add.text(5, 5,
            `
                Game over.
                ${this.message}.
                Click to play again!
            `, 
            {
                fontFamily: '"m3x6"',
                fontSize: '50px'
            });

        this.controls = new Controls(this.input);

    }

    update() {
        if (this.controls.isStart()) {

            this.scene.start('TestScene');
        }
    }
}
