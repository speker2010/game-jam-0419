import Bullet from './Bullet';
import TestScene from './test-scene';

export default class BulletExtra extends Bullet {
    constructor(scene: TestScene, x:number, y:number, texture:string) {
        super(scene, x, y, texture);
        this.setTexture('bullet_extra');
        this.damage = 50;
        this.flashKey = 'minigun_extra_flash';
        this.flashAnimKey = 'minigun_extra.shoot';
    }
}