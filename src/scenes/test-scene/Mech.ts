import Unit from './Unit';
import * as Phaser  from 'phaser';
import Container = Phaser.GameObjects.Container;
import Sprite = Phaser.GameObjects.Sprite;
import Zone = Phaser.GameObjects.Zone;
import TestScene from './test-scene';
import {Gun, TransformationInfo} from '../../types';
import Bullet from './Bullet';
import Controls from '../../components/Controls';

const ANGULAR_VELOCITY = 150;


export default class Mech extends Unit {
    public health: number;
    public headContainer: Container;
    public head: Sprite;
    public minigunRight: Zone;
    public minigunLeft: Zone;
    public scene: TestScene;
    public controls: Controls;

    constructor(scene: TestScene, x:number, y:number) {
        super(scene, x, y, 'mech_walk', 'frame1');
        this.setOrigin(0.5, 0.6);
        scene.add.existing(this);
        scene.physics.add.existing(this);
        this.scene = scene;
        this.headContainer = this.scene.add.container(this.x, this.y);
        this.head = this.scene.add.sprite(0, 0, 'mech_fire', 'frame04');
        this.minigunRight = this.scene.add.zone(60, -50, 1, 1);
        this.minigunLeft = this.scene.add.zone(-60, -50,1 ,1);        
        this.headContainer.add(this.minigunRight);
        this.headContainer.add(this.minigunLeft);
        this.headContainer.add(this.head);
        this.head.mech = this;
        this.head.setOrigin(0.5, 0.6);
        this.setCollideWorldBounds(true);
        this.setDepth(10);
        this.headContainer.setDepth(11);
        this.controls = new Controls(scene.input);
    }

    getWeapon() {
        var tempMatrix = new Phaser.GameObjects.Components.TransformMatrix();
        var tempParentMatrix = new Phaser.GameObjects.Components.TransformMatrix();
        var guns: Gun[] = [];
        this.minigunRight.getWorldTransformMatrix(tempMatrix, tempParentMatrix);
        var d: TransformationInfo = <TransformationInfo>tempMatrix.decomposeMatrix();
        guns.push({
            x: d.translateX,
            y: d.translateY,
            rotation: this.headContainer.rotation - (Math.PI / 2)
        });
        this.minigunLeft.getWorldTransformMatrix(tempMatrix, tempParentMatrix);
        d = <TransformationInfo>tempMatrix.decomposeMatrix();
        guns.push({
            x: d.translateX,
            y: d.translateY,
            rotation: this.headContainer.rotation - (Math.PI / 2)
        });
        return guns;
    }

    update(time: number, delta: number) {
        var currentSpeed = 0;
        if (this.controls.isRotateGunsLeft()) {
            this.headContainer.angle -= 0.085 * delta;
            this.scene.soundWzhh.play();
        } else if (this.controls.isRotateGunsRight()) {
            this.headContainer.angle += 0.085 * delta;
            this.scene.soundWzhh.play();
        } else {
            this.scene.soundWzhh.stop();
        }

        if (this.controls.isRotateBodyLeft()) {
            this.setAngularVelocity(-ANGULAR_VELOCITY);
        } else if (this.controls.isRotateBodyRight()) {
            this.setAngularVelocity(ANGULAR_VELOCITY);
        } else {
            this.setAngularVelocity(0);
        }

        if (this.controls.isMove()) {
            currentSpeed = 200;
            this.play('mech.walk', true);
            if (!this.scene.soundWalk.isPlaying && this.isAlive()) {
                this.scene.soundWalk.play()
            }            
        } else {
            this.play('mech.stay', true);            
            this.scene.soundWalk.stop();
        }

        if (this.controls.isFire()) {
            this.fire(time, delta);            
        }
        
        this.scene.physics.velocityFromRotation(this.rotation - (Math.PI /2), currentSpeed, this.body.velocity);

        this.headContainer.x = this.x;
        this.headContainer.y = this.y;
    }

    fire(time: number, delta: number) {
        this.head.play('mech.fire', true);
        super.fire(time, delta);
    }

    hit(unit: Unit, bullet: Bullet) {        
        if (unit.mech) {
            unit = unit.mech;
        }        
        unit.damage(bullet.damage);
        bullet.destroy();        
        if (!unit.isAlive()) {            
            
            unit.scene.gameOver('You died');            
        }
    }

    getMaxHealth(): number {
        return 10000;
    }
}
